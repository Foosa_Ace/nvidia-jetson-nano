# Quick Links #
* [Official User guide](https://developer.download.nvidia.com/assets/embedded/secure/jetson/Nano/docs/NVIDIA_Jetson_Nano_Developer_Kit_User_Guide.pdf?8kIhl-1Lm8VRTxc20zkc6TtoxiJYQ-M7HvxzmXXIYMsm1QdAfx8ZBRhEf0qsejc_TY8UypFUBodXLdRdXnquv6lLgGnMiMqjmH9LswWgX2D6m-jaBa9bvQyaxXsfQHm8zU6zqBAMxFF3oVNTcRb-ntbQdQjUmGBphwg0BefMKUv1jWUk32IhSC-tAjGw55fJeTw4kp2a)
* [Getting started with the NVIDIA Jetson Nano](https://www.pyimagesearch.com/2019/05/06/getting-started-with-the-nvidia-jetson-nano/)

# SETUP #

* [Getting started](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#intro)