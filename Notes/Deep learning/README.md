The NN runs on two deep neural networks - Base network and detection network

#### Base network ####

* MobileNet
* 



#### Deep Learning Frameworks: ####

* Caffe - Oldest, more models -                                                 NO
* Caffe2 - Next gen caffe, has good model support -                             CONSIDERED
* Chainer - Not very popular, famous for NLP -                                  NO
* CNTK(Microsoft Cognitive Toolkit) - License problem -                         NO
* Deeplearning4j - Java -                                                       NO
* Keras - "Probably the best Python API" can run on TF and Theano -             CONSIDERED
* MATLAB -                                                                      NO
* MxNet -                                                                       NO
* TensorFlow - Using Keras -                                                    CONSIDERED
* Theano - Using Keras                                                          CONSIDERED
* Torch/PyTorch - Good support and easier than tf -                             CONSIDERED  

### Object detection algorithms: ###

* YOLOv3 - 
* Faster R-CNN
* R-FCN
* SSD
* FPN
* RetinaNET

[Reference](https://medium.com/@jonathan_hui/object-detection-speed-and-accuracy-comparison-faster-r-cnn-r-fcn-ssd-and-yolo-5425656ae359)

### Object tracking algorithms: ###

* SORT
* 
* 

[VOT - Visual Object Tracking](http://www.votchallenge.net/)